let max = 10;

for (i = 0; i <= max; i++) {
    for (x = 0; x <= max; x++) {
        if (i == 0 || i == max || x == 0 || x == max) {
            document.write("*");
        } else {
            document.write("&nbsp&nbsp");
        }
    }
    document.write("<br>");
}

for (i = 0; i <= max; i++) {
    for (x = 0; x <= max; x++) {
        if (x < (max - i)) {
            document.write("&nbsp");
        } else {
            document.write("*");
        }
    }
    document.write("<br>");
}

for (i = 0, x = ""; i <= max; i++) {
    x = x + "*";
    document.write(x + "<br>");
}

for (i = 0; i <= max; i++) {
    for (x = 0; x <= max/2; x++) {
        if (x < Math.abs(max/2 - i)) {
            document.write("&nbsp");
        } else {
            document.write("*");
        }
    }
    document.write("<br>");
}